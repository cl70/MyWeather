package se.ckea.myweather;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import se.ckea.myweather.provider.LocationRecord;

/**
 * Created by cl on 17/10/31.
 */

public class LocationRecordTest {
    private static final String aLocation = "HomeTown";
    private static final String aStationKey = "12023";
    private static final float  aTempValue = 20.0f;
    private static final float  newTempValue = 22.2f;
    private static final float  floatDelta = 0.04f;
    private static final int    aLocationId = 100;

    private LocationRecord location = new LocationRecord(aLocation, aStationKey, aTempValue, true);
    private LocationRecord locationTempNotValid = new LocationRecord(aLocation, aStationKey);
    private LocationRecord locationIdSet = new LocationRecord(aLocationId, aLocation, aStationKey, aTempValue, true);

    @Test
    public void uninitalizedTempIsNotValid() {
        assertFalse(locationTempNotValid.isTemperatureValid());
    }

    @Test
    public void initalizedTempIsValid() {
        assertTrue(location.isTemperatureValid());
    }

    @Test
    public void modifyTempValueMakesTempValid() {
        locationTempNotValid.setTemperature(aTempValue);
        assertTrue(locationTempNotValid.isTemperatureValid());
    }

    @Test
    public void modifyTempValidityMakesTempValid() {
        locationTempNotValid.setTemperatureValid(true);
        assertTrue(locationTempNotValid.isTemperatureValid());
    }

    @Test
    public void modifyTempValidityMakesTempInvalid() {
        location.setTemperatureValid(false);
        assertFalse(locationTempNotValid.isTemperatureValid());
    }

    @Test
    public void getLocationName() {
        assertEquals(aLocation, location.getName());
    }

    @Test
    public void getTemperature() {
        assertEquals(aTempValue, location.getTemperature(),floatDelta);
    }
    @Test
    public void setTemperature() {
        location.setTemperature(newTempValue);
        assertEquals(newTempValue, location.getTemperature(), floatDelta);
    }

    @Test
    public void getRecordId() {
        assertEquals(aLocationId, locationIdSet.getId());
    }
}
