package se.ckea.myweather;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * Created by cl on 17/11/02.
 */

@PrepareForTest({WeatherOps.class, Log.class})
@RunWith(PowerMockRunner.class)
public class JSONParseTest {

    JSONParse parseObj = new JSONParse();
    JSONParse spyParseObj;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Log.class);
        spyParseObj = Mockito.spy(parseObj);
    }

    // ToDo: Finalize test
    @Test
    @Ignore
    public void getParametersSuccess() {
        try {
            String jsonData = "{\"key\":\"latest\",updated\":1509850800000,\"title\":\"Senaste...\"}";
            JSONObject jsonObj = new JSONObject(jsonData);
            when(spyParseObj.readJsonFromUrl(Mockito.anyString())).thenReturn(jsonObj);

            Log.v("AA", "BB");
            String paramStr = spyParseObj.getParameters();

        } catch (JSONException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
