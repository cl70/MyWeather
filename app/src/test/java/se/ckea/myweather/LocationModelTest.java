package se.ckea.myweather;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;

import se.ckea.myweather.provider.LocationRecord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by cl on 17/10/08.
 */

@PrepareForTest({WeatherOps.class, Log.class})
@RunWith(PowerMockRunner.class)
public class LocationModelTest {
    LocationModel mLocationModel;

    @Mock
    WeatherOps weatherOps;

    @Mock
    ArrayList<LocationRecord> mockLocationArr;

    @Before
    public void initLocationModel() {
        PowerMockito.mockStatic(WeatherOps.class);
        PowerMockito.mockStatic(Log.class);

        PowerMockito.when(WeatherOps.getInstance()).thenReturn(weatherOps);

        mLocationModel = LocationModel.getInstance();
    }

    @Test
    public void checkState_Uninitalized() {
        assertEquals(LocationModel.State.UNINITIALIZED, mLocationModel.getState());
    }
}
