package se.ckea.myweather;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by cl on 17/10/31.
 */

@PrepareForTest({WeatherService.class, Log.class})
@RunWith(PowerMockRunner.class)
public class AlarmReceiverTest {

    AlarmReceiver receiver = new AlarmReceiver();

    @Mock
    Intent intent;

    @Mock
    Intent weatherIntent;

    @Mock
    Context context;

    @Test
    public void startServiceCalledOnReceive() {
        PowerMockito.mockStatic(WeatherService.class);
        PowerMockito.mockStatic(Log.class);

        when(WeatherService.makeIntent(context)).thenReturn(weatherIntent);

        receiver.onReceive(context, intent);

        verify(context).startService(weatherIntent);
    }
}
