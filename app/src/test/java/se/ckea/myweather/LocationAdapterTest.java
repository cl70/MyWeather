package se.ckea.myweather;

import android.content.Context;
import android.test.mock.MockContext;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import se.ckea.myweather.provider.LocationRecord;


/**
 * Created by cl on 17/10/06.
 */

@RunWith(MockitoJUnitRunner.class)
public class LocationAdapterTest {
    private LocationAdapter mAdapter;

    private static final String[] locNames = {"Göteborg", "Karlstad", "Grebbestad", "Sollid"};
    private static final String[] locKeys = {"12030", "12031", "12032", "12033"};
    private static final float[] locTemps = { 24.45f, 20f, 18f, 19.67f};
    private static ArrayList<LocationRecord> mLocations;

    @Mock
    LayoutInflater mockInflater;

    @Mock
    Context mockContext;

    @Mock
    TextView mockTextView;

    @Mock
    ViewGroup mMockParent;

    int resource;

    // ToDo: Adapt test to CursorAdapter
    @Before
    public void initLocations() {
        mockContext = new MockContext();

        mLocations = new ArrayList<LocationRecord>();
        for (int i=0; i<locNames.length; i++) {
            mLocations.add(new LocationRecord(locNames[i], locKeys[i], locTemps[i]));
        }
        when(LayoutInflater.from(mockContext)).thenReturn(mockInflater);
 //       mAdapter = new LocationAdapter(mockContext, resource, mLocations);

    }

    @Test
    @Ignore
    public void locationAdapter_GetView_returnsTempString() {
        int i = 0;
        TextView textView = (TextView) mAdapter.getView(i, null, mMockParent);
        String gbgTemp = Float.toString(locTemps[i]) + "°C";
        assertEquals(gbgTemp, textView.getText());

    }
}
