package se.ckea.myweather;

import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by cl on 17/11/01.
 */

@PrepareForTest({WeatherOps.class, Log.class})
@RunWith(PowerMockRunner.class)
public class WeatherServiceTest {

    @Mock
    WeatherOps weatherOps;

    @Mock
    Intent intent;

    @Mock
    JSONParse openDataMetobsReader;

    WeatherService service;

    String locationArr[] = {"12030", "12031", "12032", "12033"};
    String tempStr = "20.5";

    @Before
    public void setUp() {
        PowerMockito.mockStatic(WeatherOps.class);
        PowerMockito.mockStatic(Log.class);

        when(WeatherOps.getInstance()).thenReturn(weatherOps);
        service = new WeatherService();
    }
    @Test
    public void tempValuesReceivedOnQuery() {
        WeatherService spyService = Mockito.spy(service);
        Mockito.doReturn(locationArr).when(spyService).queryForStationKeys();
        Mockito.doReturn(tempStr).when(spyService).getTempValue(Mockito.anyString());
        try {
            when(openDataMetobsReader.getData(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(tempStr);
        } catch (IOException e) {
            e.printStackTrace();
        }

        spyService.onHandleIntent(intent);

        try {
            verify(weatherOps, times(locationArr.length)).updateTemperatureByStationKey(Mockito.anyString(), Mockito.anyFloat(), Mockito.anyBoolean());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void extractTempValueFromReceivedData() {
        String expectedTemp = "12.0";
        // Valid data
        String data = "\uFEFFStationsnamn;Klimatnummer;Mäthöjd (meter över marken)\n" +
                " Måseskär A;81050;2.0\n" +
                " \n" +
                " Parameternamn;Beskrivning;Enhet\n" +
                " Lufttemperatur;momentanvärde, 1 gång/tim;degree celsius\n" +
                " \n" +
                " Tidsperiod (fr.o.m);Tidsperiod (t.o.m);Höjd (meter över havet);Latitud (decimalgrader);Longitud (decimalgrader)\n" +
                " 1995-11-01 00:00:00;2017-11-02 03:00:00;16.0;58.094;11.3333\n" +
                " 1995-08-01 00:00:00;1995-10-31 23:59:59;16.0;58.0941;11.3335\n" +
                " \n" +
                "Datum;Tid (UTC);Lufttemperatur;Kvalitet;;Tidsutsnitt:\n" +
                " 2017-11-02;03:00:00;" + expectedTemp + ";Y;;Data från senaste timmen\n" +
                " ;;;;;Tidsperiod (fr.o.m.) = 2017-11-02 02:00:01 (UTC";

        String tempStr = service.getTempValue(data);
        assertEquals("Extracted temperature differs from expected", expectedTemp, tempStr);

    }

    @Test
    public void extractTempValueFromInvalidReceivedData() {
        String expectedTemp = "12.0";
        // data with no header row
        String data = "\uFEFFStationsnamn;Klimatnummer;Mäthöjd (meter över marken)\n" +
                " Måseskär A;81050;2.0\n" +
                " \n" +
                " Parameternamn;Beskrivning;Enhet\n" +
                " Lufttemperatur;momentanvärde, 1 gång/tim;degree celsius\n" +
                " \n" +
                " Tidsperiod (fr.o.m);Tidsperiod (t.o.m);Höjd (meter över havet);Latitud (decimalgrader);Longitud (decimalgrader)\n" +
                " 1995-11-01 00:00:00;2017-11-02 03:00:00;16.0;58.094;11.3333\n" +
                " 1995-08-01 00:00:00;1995-10-31 23:59:59;16.0;58.0941;11.3335\n" +
                " \n" +
                " 2017-11-02;03:00:00;" + expectedTemp + ";Y;;Data från senaste timmen\n" +
                " ;;;;;Tidsperiod (fr.o.m.) = 2017-11-02 02:00:01 (UTC";

        String tempStr = service.getTempValue(data);
        assertEquals("Extracted temperature should be empty string when extraction fails", "", tempStr);

    }

}
