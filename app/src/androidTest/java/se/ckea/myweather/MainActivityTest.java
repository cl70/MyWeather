package se.ckea.myweather;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ListView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


/**
 * Created by cl on 17/10/08.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    int mNofLocations;
    ListView mListView;

    @Before
    public void setUp() {
        onView(withId(R.id.settings_button))
                .perform(click());
        onView(withId(R.id.remove_locations))
                .perform(click());
        onView(withId(R.id.init_locations))
                .perform(click());
        pressBack();
        onView(withId(R.id.update_button))
                .perform(click());
        mListView  = (ListView) mActivityRule.getActivity().findViewById(R.id.location_list);
        mNofLocations = mListView.getAdapter().getCount();
    }

    @Test
    public void list_is_populated() {
        assertEquals(4, mNofLocations);
    }

}
