package se.ckea.myweather;

/**
 * Interface for notifying a listener when the weather data is updated.
 */

public interface OnWeatherChangedListener {
    void onWeatherChanged();
}
