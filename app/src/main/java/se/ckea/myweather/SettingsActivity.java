package se.ckea.myweather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

/**
 * The `SettingsActivity` presents a number of options to configure MyWeather.
 *  1. Displayed temperature unit, °C or °F radio button selection.
 *  2. Interval for data retrieval.
 *  3. Button to initialize the `TemperatureProvider` with a set of hardcoded locations.
 *  4. Button to remove all locations from the 'TemperatureProvider'
 */

public class SettingsActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();

    public static final String KEY_PREF_FAHRENHEIT = "pref_temp_unit";
    public static final String KEY_PREF_UPDATE_INTERVAL = "pref_update_interval";

    private LocationModel mLocationModel;

    public static Intent makeIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mLocationModel = LocationModel.getInstance();
    }

    /** Called when the user taps the Add Locations button */
    public void onAddLocationsClick(View view) {
        Log.i(TAG, "onAddLocationsClick");
        if (mLocationModel != null) {
            mLocationModel.initalizeLocations();
        }
    }

    /** Called when the user taps the Remove Locations button */
    public void onRemoveLocationsClick(View view) {
        Log.i(TAG, "onRemoveLocationsClick");
        if (mLocationModel != null) {
            mLocationModel.removeAllLocations();
        }
    }
}
