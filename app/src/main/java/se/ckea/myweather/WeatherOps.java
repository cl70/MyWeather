package se.ckea.myweather;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;

import se.ckea.myweather.provider.LocationContract;


/**
 * Support class that consolidates and simplifies operations on the
 * content provider WeatherProvider.
 */

public class WeatherOps  extends ContentObserver {
    private String TAG = getClass().getSimpleName();

    /**
     * Contains the most recent result from a query so the display can
     * be updated after a runtime configuration change.
     */
    private Cursor mCursor;

    /**
     * Define the Proxy for accessing the WeatherProvider.
     */
    private ContentResolver mCr;


    private static WeatherOps instance;

    /**
     * A private constructor so no instances can be made outside this class
     */
    private WeatherOps() {
        super(new Handler(Looper.getMainLooper()));
    }


    public static synchronized WeatherOps getInstance() {
        if(instance == null)
            instance = new WeatherOps();

        return instance;
    }

    // The init method initializes the fields.
    public void init(MainActivity weatherActivity) {
        mCr = weatherActivity.getContentResolver();
        mCr.registerContentObserver(LocationContract.LocationEntry.CONTENT_URI, false, this);
    }

    /**
     * Insert a Location @a location name into
     * the WeatherProvider.
     */
    public Uri insert(String location) throws RemoteException {
        final ContentValues cvs = new ContentValues();

        // Insert data.
        cvs.put(LocationContract.LocationEntry.COLUMN_NAME, location);
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE, 0.0f);
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID, 0);

        // Insert the content at the designated URI.
        return insert(LocationContract.LocationEntry.CONTENT_URI,
                cvs);
    }

    /**
     * Insert @a ContentValues into the WeatherProvider at
     * the @a uri.
     */
    protected Uri insert(Uri uri, ContentValues cvs) {
        return mCr.insert(uri, cvs);
    }

    /**
     * Insert an array of locations @a name
     * into the WeatherProvider.
     */
    public int bulkInsert(String[] names, String[] stationKeys) throws RemoteException {
        if (names.length != stationKeys.length) {
            Log.e(TAG, "bulkInsert() - Input parameters have different length");
            return 0;
        }
        // Use ContentValues to store the values in appropriate
        // columns, so that ContentResolver can process it.  Since
        // more than one rows needs to be inserted, an Array of
        // ContentValues is needed.
        ContentValues[] cvsArray =
                new ContentValues[names.length];

        // Insert all the characters into the ContentValues array.
        for (int i=0; i<names.length; i++) {
            ContentValues cvs = new ContentValues();
            cvs.put(LocationContract.LocationEntry.COLUMN_NAME, names[i]);
            cvs.put(LocationContract.LocationEntry.COLUMN_STATION_KEY, stationKeys[i]);
            cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE, 0.0f);
            cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID, 0);
            cvsArray[i] = cvs;
        }

        // Insert the array of content at the designated URI.
        return bulkInsert
                (LocationContract.LocationEntry.CONTENT_URI,
                        cvsArray);
    }

    /**
     * Insert an array of @a ContentValues into the
     * WeatherProvider at the @a uri.
     */
    protected int bulkInsert(Uri uri,
                             ContentValues[] cvsArray) {
        return mCr.bulkInsert(uri,
                cvsArray);
    }

    /**
     * Return a Cursor from a query on the WeatherProvider at
     * the @a uri.
     */
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        return mCr.query(uri,
                projection,
                selection,
                selectionArgs,
                sortOrder);

    }

    /**
     * Update the @a temperature for a location at a
     * designated @a uri from the WeatherProvider.
     */
    public int updateTemperatureByUri(Uri uri,
                           float temperature) throws RemoteException {
        // Initialize the content values.
        final ContentValues cvs = new ContentValues();
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE, temperature);
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID, 1);

        // Update the content at the designated URI.
        return update(uri,
                cvs,
                null,
                null);
    }

    /**
     * Update the @a temperature of a location with a given
     * @a name in the WeatherProvider.
     */
    public int updateTemperatureByName(String name,
                                       float temperature,
                                       boolean tempValid) throws RemoteException {
        // Initialize the content values.
        final ContentValues cvs = new ContentValues();
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE, temperature);
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID, tempValid ? 1 : 0);

        // Update the content at the designated URI.
        return update(LocationContract.LocationEntry.CONTENT_URI,
                cvs,
                LocationContract.LocationEntry.COLUMN_NAME,
                new String[] { name });
    }

    /**
     * Update the @a temperature of a location with a given
     * @a station key in the WeatherProvider.
     */
    public int updateTemperatureByStationKey(String stationKey,
                                             float temperature,
                                             boolean tempValid) throws RemoteException{
        // Initialize the content values.
        final ContentValues cvs = new ContentValues();
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE, temperature);
        cvs.put(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID, tempValid ? 1 : 0);

        // Update the content at the designated URI.
        return update(LocationContract.LocationEntry.CONTENT_URI,
                cvs,
                LocationContract.LocationEntry.COLUMN_STATION_KEY,
                new String[] { stationKey });
    }

    /**
     * Update the @a selection and @a selectionArgs with the @a
     * ContentValues in the WeatherProvider at the @a uri.
     */
    public int update(Uri uri,
                      ContentValues cvs,
                      String selection,
                      String[] selectionArgs) {
        return mCr.update(uri,
                cvs,
                selection,
                selectionArgs);
    }

    /**
     * Delete an array of locations @a locationNames from the
     * WeatherProvider.
     */
    public int deleteByName(String[] locationNames)
            throws RemoteException {
        return delete(LocationContract.LocationEntry.CONTENT_URI,
                LocationContract.LocationEntry.COLUMN_NAME,
                locationNames);
    }

    /**
     * Delete the @a selection and @a selectionArgs from the
     * WeatherProvider at the @a uri.
     */
    protected int delete(Uri uri,
                         String selection,
                         String[] selectionArgs) {
        return mCr.delete
                (uri,
                        selection,
                        selectionArgs);
    }

    /**
     * Delete all locations from the WeatherProvider.
     */
    public int deleteAll()
            throws RemoteException {
        return delete(LocationContract.LocationEntry.CONTENT_URI,
                null,
                null);
    }

    /**
     * Display the current contents of the WeatherProvider.
     */
    public void displayAll()
            throws RemoteException {
        // Query for all characters in the WeatherProvider by their race.
        mCursor = query(LocationContract.LocationEntry.CONTENT_URI,
                LocationContract.LocationEntry.sColumnsToDisplay,
                null,
                null,
                null);

        if (mCursor.getCount() == 0) {
            mCursor = null;
        }
        // Display the results of the query.
        LocationModel.getInstance().setCursor(mCursor);
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Log.i(TAG, "onChange()");
        try {
            displayAll();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
