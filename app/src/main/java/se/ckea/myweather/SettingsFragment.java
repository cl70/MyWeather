package se.ckea.myweather;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Loads a resource for displaying and modifying the settings via persistent storage.
 */

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
