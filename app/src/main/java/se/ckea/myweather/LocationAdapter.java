package se.ckea.myweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.ckea.myweather.provider.LocationRecord;

/**
 * The `LocationAdapter` class extends `CursorAdapter` and is used for populating the list
 * in `MainActivity`.
 */

public class LocationAdapter extends CursorAdapter {
    private String TAG = getClass().getSimpleName();

    private LayoutInflater layoutInflater;
    private Context context;

    LocationAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    // Cursor adapter
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(R.layout.list_item_2_columns, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name;
        TextView temp;

        name = (TextView) view.findViewById(R.id.location);
        temp = (TextView) view.findViewById(R.id.temperature);

        LocationRecord loc = LocationRecord.fromCursor(cursor);

        name.setText(loc.getName());

        String tempStr;
        if (loc.isTemperatureValid()) {
            float tempValue = loc.getTemperature();
            tempValue = convertUnit(tempValue);
            tempStr = String.format("%.1f", tempValue);
        }
        else {
            tempStr = "--.-";
        }
        tempStr = tempStr + addUnit();
        temp.setText(tempStr);
    }
    // End cursor adapter

    private String addUnit() {
        String res = "°C";

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean tempInFahrenheit = sharedPref.getBoolean(SettingsActivity.KEY_PREF_FAHRENHEIT, false);

        if (tempInFahrenheit) {
            res = "°F";
        }
        return res;
    }

    private float convertUnit(float tempValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean tempInFahrenheit = sharedPref.getBoolean(SettingsActivity.KEY_PREF_FAHRENHEIT, false);

        if (tempInFahrenheit) {
            tempValue = 1.8f*tempValue + 32.0f;
        }
        return tempValue;
    }
}
