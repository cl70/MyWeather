package se.ckea.myweather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * The `AlarmReceiver` is triggered with an interval that can be configured via the `SettingsActivity`.
 * The receiver initiates updating of current temperatures by starting the `WeatherService`.
 */

public class AlarmReceiver extends BroadcastReceiver {
    private String TAG = getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Date date = Calendar.getInstance().getTime();
        String timeFormatted = new SimpleDateFormat("yyyy-MM-dd - HH:mm:ss", Locale.getDefault())
                .format(date);
        Log.i(TAG, "onReceive() at " + timeFormatted);

        Intent serviceIntent = WeatherService.makeIntent(context);
        context.startService(serviceIntent);
    }
}
