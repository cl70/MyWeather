package se.ckea.myweather;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import se.ckea.myweather.provider.LocationRecord;

/**
 * The `LocationModel` class holds HMI related information such as status and time for latest data
 * retrieval. It gets notified when the `WeatherProvider` has been updated. The `LocationModel`
 * is then responsible for notifying the `MainActivity` that the weather data is updated. The class
 * also holds the hardcoded data necessary to initialize the provider with a set of stations.
 *
 * The `LocationModel` is implemented as a singelton.
 *
 * The `OnWeatherChanged` interface is used for notifying that the weather data is updated.
 */

public class LocationModel {
    // Hardcoded Location data used for initalizing the content provider database
    private static String[] names = {"Göteborg", "Karlstad", "Väderöarna", "Måseskär"};
    private static String[] stationKeys =  {"71420", "93220", "81350", "81050"};

    public enum State {
        UNINITIALIZED, INITIALIZED, UPDATE_OK, UPDATE_PARTIALLY_OK, UPDATE_FAILED
    }

    private String TAG = getClass().getSimpleName();

    private static LocationModel instance = null;

    private Cursor mCursor;
    private WeatherOps mWeatherOps;
    private State mState;
    private Date  mTimeForLastUpdate;
    private OnWeatherChangedListener mListener;

    //a private constructor so no instances can be made outside this class
    private LocationModel() {
        mCursor = null;
        mWeatherOps = WeatherOps.getInstance();
        // ToDo: This is not really time for update and state may not be uninitalized
        mState = State.UNINITIALIZED;
        updateTime();
        mListener = null;
    }


    public static synchronized LocationModel getInstance() {
        if(instance == null)
            instance = new LocationModel();

        return instance;
    }

    /** initalizeLocations
     * initalizeLocations inserts the hardcoded stations to the WeatherProvider. If there are no
     * stations in the provider then the WeatherService will not retrieve any data.
     */
    public void initalizeLocations() {
        try {
            mWeatherOps.bulkInsert(names, stationKeys);
        } catch (RemoteException e) {
            Log.w(TAG, "exception " + e);
        }
        mState = State.INITIALIZED;
        updateTime();
    }

    public void removeAllLocations() {
        try {
            mWeatherOps.deleteAll();
        } catch (RemoteException e) {
            Log.w(TAG, "exception " + e);
        }
        mState = State.UNINITIALIZED;
        updateTime();
    }

    public void setOnWeatherChangedListener(OnWeatherChangedListener listener) {
        mListener = listener;
    }

    /** getCursor
     * Used by main activity to update its list of location
     * @return
     */
    public Cursor getCursor() {
        return mCursor;
    }

    /** getState
     * Returns the state of the temperature data to indicate wether last temperature retrieval
     * were successful or not.
     * @return
     */
    public State getState() { return mState; }

    public Date getTimeForLastUpdate() {
        return mTimeForLastUpdate;
    }

    /** setCursor
     * setCursor is called whenever a retrieval of temperature data from the content provider has
     * occured.
     * @param cursor
     */
    public void setCursor(Cursor cursor) {
        mCursor = cursor;
        updateState();
        updateTime();
        notifyListeners();
    }

    /** updateState
     * Update the overall state for temperature data retrieval, based on all locations.
     */
    private void updateState() {
        if (mCursor == null) {
            Log.w(TAG, "updateState() . mCursor == null");
            return;
        }
        State tmpState = State.UPDATE_OK;
        boolean okTempFound = false;
        mCursor.moveToFirst();
        do {
            LocationRecord record =
                    LocationRecord.fromCursor(mCursor);
            if (record.isTemperatureValid()) {
                okTempFound = true;
            }
            else {
                tmpState = State.UPDATE_PARTIALLY_OK;
            }
        } while (mCursor.moveToNext());
        if (!okTempFound) {
            tmpState = State.UPDATE_FAILED;
        }
        mState = tmpState;
    }

    // ToDo: Time updates dosn't reflect when data was actually written to content provider.
    private void updateTime() {
        mTimeForLastUpdate = Calendar.getInstance().getTime();
    }

    private void notifyListeners() {
        if (mListener != null) {
            mListener.onWeatherChanged();
        }
        else {
            Log.w(TAG, "notifyListeners - no listener set!");
        }
    }
}
