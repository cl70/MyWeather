package se.ckea.myweather.provider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * This contract defines the metadata for the WeatherContentProvider,
 * including the provider's access URIs and its "database" constants.
 */
public class LocationContract {
    /**
     * This ContentProvider's unique identifier.
     */
    public static final String CONTENT_AUTHORITY =
            "se.ckea.weatherprovider";

    /**
     * Use CONTENT_AUTHORITY to create the base of all URI's which
     * apps will use to contact the content provider.
     */
    public static final Uri BASE_CONTENT_URI =
            Uri.parse("content://"
                    + CONTENT_AUTHORITY);

    /**
     * Possible paths (appended to base content URI for possible
     * URI's), e.g., a valid path for looking at Character data is
     * content://se.ckea.weatherprovider/location_table .
     * However, content://se.ckea.weatherprovider/givemeroot
     * will fail, as the ContentProvider hasn't been given any
     * information on what to do with "givemeroot".
     */
    public static final String PATH_LOCATION =
            LocationEntry.TABLE_NAME;

    /*
     * Columns
     */

    /**
     * Inner class that defines the table contents of the Weather
     * table.
     */
    public static final class LocationEntry
            implements BaseColumns {
        /**
         * Use BASE_CONTENT_URI to create the unique URI for Acronym
         * Table that apps will use to contact the content provider.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon()
                        .appendPath(PATH_LOCATION).build();

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 0..x items.
         */
        public static final String CONTENT_ITEMS_TYPE =
                "vnd.android.cursor.dir/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_LOCATION;

        /**
         * When the Cursor returned for a given URI by the
         * ContentProvider contains 1 item.
         */
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/"
                        + CONTENT_AUTHORITY
                        + "/"
                        + PATH_LOCATION;

        /**
         * Columns to display.
         */
        public static final String sColumnsToDisplay [] =
                new String[] {
                        LocationContract.LocationEntry._ID,
                        LocationContract.LocationEntry.COLUMN_NAME,
                        LocationContract.LocationEntry.COLUMN_STATION_KEY,
                        LocationContract.LocationEntry.COLUMN_TEMPERATURE,
                        LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID
                };

        /**
         * Name of the database table.
         */
        public static final String TABLE_NAME =
                "location_table";

        /**
         * Columns to store data.
         */
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_STATION_KEY = "station_key";
        public static final String COLUMN_TEMPERATURE = "temperature";
        public static final String COLUMN_TEMPERATURE_VALID = "temperature_valid";

        /**
         * Return a Uri that points to the row containing a given id.
         *
         * @param id row id
         * @return Uri URI for the specified row id
         */
        public static Uri buildUri(Long id) {
            return ContentUris.withAppendedId(CONTENT_URI,
                    id);
        }
    }
}
