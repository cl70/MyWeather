package se.ckea.myweather.provider;

import android.database.Cursor;
import android.support.annotation.NonNull;

/**
 * A simple POJO that stores weather related information for locations.
 */
public class LocationRecord {
    /**
     * Start at 0.
     */
    static long sInitialId = 0;

    /**
     * Id of the location in the map.
     */
    private final long mId;

    /**
     * Name of the location.
     */
    private final String mName;

    /**
     * Station key for the location. Station key is used when retrieving data from SMHI.
     */
    private final String mStationKey;

    /**
     * Current temperature for the location
     */
    private float mTemperature;

    /**
     * Indicates validity for the temperature
     */
    private boolean mTemperatureValid;

    /**
     * Constructor initializes the name and stationKey fields.
     */
    public LocationRecord(String name, String stationKey) {
        mName = name;
        mStationKey = stationKey;
        mTemperature = 0.0f;
        mTemperatureValid = false;
        mId = ++sInitialId;
    }

    /**
     * Constructor initializes the name and temperature fields.
     */
    public LocationRecord(String name,
                   String stationKey,
                   float temperature) {
        mName = name;
        mStationKey = stationKey;
        mTemperature = temperature;
        mTemperatureValid = true;
        mId = ++sInitialId;
    }

    /**
     * Constructor initializes all data fields.
     */
    public LocationRecord(String name,
                   String stationKey,
                   float temperature,
                   boolean tempValid) {
        mName = name;
        mStationKey = stationKey;
        mTemperature = temperature;
        mTemperatureValid = tempValid;
        mId = ++sInitialId;
    }

    /**
     * Constructor initializes all the fields.
     */
    public LocationRecord(long id,
                   String name,
                   String stationKey,
                   float temperature,
                   boolean tempValid) {
        mId = id;
        mName = name;
        mStationKey = stationKey;
        mTemperature = temperature;
        mTemperatureValid = tempValid;
    }

    /**
     * Constructor initializes all fields from a cursor.
     */
    public LocationRecord(@NonNull Cursor cursor) {
        mId = cursor.getInt(cursor.getColumnIndex(LocationContract.LocationEntry._ID));
        mName = cursor.getString(cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_NAME));
        mStationKey = cursor.getString(cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_STATION_KEY));
        mTemperature = cursor.getFloat(cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_TEMPERATURE));
        int nValid = cursor.getInt(cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_TEMPERATURE_VALID));
        mTemperatureValid = (nValid == 1);
    }


    /**
     * Static builder method returns a new character record from a given cursor.
     */
    public static LocationRecord fromCursor(Cursor cursor) {
        return new LocationRecord(cursor);
    }

    /**
     * @return the id of the location.
     */
    public long getId() {
        return mId;
    }

    /**
     * @return the name of the location.
     */
    public String getName() {
        return mName;
    }

    /**
     * @return the current temperature.
     */
    public float getTemperature() {
        return mTemperature;
    }

    /**
     * Set the current temperature for the location.
     */
    public void setTemperature(float temp) {
        mTemperature = temp;
        mTemperatureValid = true;
    }

    /**
     * @return the validity for the temperature value
     */
    public boolean isTemperatureValid() { return mTemperatureValid; }

    /**
     * Set the validity for the temperature value
     */
    public void setTemperatureValid(boolean valid) {
        mTemperatureValid = valid;
    }
}
