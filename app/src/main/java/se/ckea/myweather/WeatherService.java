package se.ckea.myweather;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import se.ckea.myweather.provider.LocationContract;
import se.ckea.myweather.provider.LocationRecord;

/**
 * The `WeatherService` is a StartedService that retrieves all locations from the `WeatherProvider`
 * and then uses the SMHI web service for fetching the current temperatures. Finally it also updates
 * the `WeatherProvider` with the new temperatures.
 */

public class WeatherService extends IntentService {
    private String TAG = getClass().getSimpleName();

    private WeatherOps mWeatherOps;

    public static Intent makeIntent(Context context) {
        return new Intent(context, WeatherService.class);
    }

    public WeatherService() {
        super("WeatherService");
        mWeatherOps = WeatherOps.getInstance();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.i(TAG, "onHandleIntent()");

        String stationKeys[] = queryForStationKeys();

        try {
            JSONParse openDataMetobsReader = new JSONParse();
            String parameterKey = "1"; // Instantaneous air temperature observations
            String periodName = "latest-hour";
            Log.v(TAG, "Received new temperatures:");
            for (String station : stationKeys) {
                String data;
                String tempStr;
                try {
                    data = openDataMetobsReader.getData(parameterKey, station, periodName);
                    tempStr = getTempValue(data);
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Failed to get data for " + station);
                    data = "-100";
                    tempStr = ""; // Empty string indicates failure
                }
                float newTemp = 0.0f;
                boolean newTempValid = true;
                if (tempStr.equals("")) {
                    newTempValid = false;
                }
                else {
                    newTemp = Float.parseFloat(getTempValue(data));
                }
                Log.v(TAG, "Station: " + station + ", Temp: " + tempStr + ", Temp valid: " + newTempValid);
                mWeatherOps.updateTemperatureByStationKey(station, newTemp, newTempValid);
            }
            //test();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public String getTempValue(String data) {
        String[] headerRow = {"Datum", "Tid (UTC)", "Lufttemperatur", "Kvalitet"};
        String splitDataIntoRows = "\n";
        String splitRowIntoElements = ";";
        String[] elements;
        boolean dataRowFound = false;
        //Log.v(TAG, data);
        String [] rows = data.split(splitDataIntoRows);
        for (String row : rows) {
            elements = row.split(splitRowIntoElements);
            if (dataRowFound) {
                // Third element is the temperature string
                return elements[2];
            }
            if (elements.length >= headerRow.length) {
                dataRowFound = true;
                for (int i = 0; i < headerRow.length; i++) {
                    if (!headerRow[i].equals(elements[i])) {
                        dataRowFound = false;
                        break;
                    }
                }
            }
        }
        // Data row is not found and therefore also no temperature is found
        // Return empty string to indicate no temperature value
        return "";
    }

    /**
     * test() is used for browsing the SMHI database and find parameters to use in the
     * real requests for temperature values.
     */
    private void test() {
        try {
            JSONParse openDataMetobsReader = new JSONParse();
            String parameterKey = "1"; // Instantaneous air temperature observations
            String[] stationKey = {"93220", "93250", "93230", "71420"};
            String periodName = "latest-hour";
            parameterKey = openDataMetobsReader.getParameters();
            //String stationKey = openDataMetobsReader.getStationNames(parameterKey);
            /*for (String key : stationKey) {
                periodName = openDataMetobsReader.getPeriodNames(parameterKey, key);
                Log.v(TAG, key + ": " + periodName);
            }*/
            String data = openDataMetobsReader.getData(parameterKey, stationKey[0], periodName);
            Log.v(TAG, data);
        } catch (IOException e) { // | JSONException
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String[] queryForStationKeys() {
        String keys[] = null;
        Cursor cursor = mWeatherOps.query(LocationContract.LocationEntry.CONTENT_URI,
                new String[] {LocationContract.LocationEntry.COLUMN_STATION_KEY},
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            keys = new String[cursor.getCount()];
            int i = 0;
            do {
                keys[i] = cursor.getString(cursor.getColumnIndex(LocationContract.LocationEntry.COLUMN_STATION_KEY));
                i++;
            } while (cursor.moveToNext());
        }

        return keys;
    }
}
