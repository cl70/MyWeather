package se.ckea.myweather;

import android.app.AlarmManager;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.icu.util.TimeZone;
import android.os.RemoteException;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import se.ckea.myweather.provider.LocationContract;

/** The `MainActivity` is the view presenting the current temperature for all locations in a list.
 *       It also shows the time for the last retrieval of temperatures.
 *       There are two buttons:
 *       1. Update, which starts `WeatherServiece` so that new data is retrieved
 *       2. Settings, which displays the SettingsActivity.
 *
 *       All temperature data is retrieved from `TemperatureProvider` via the LocationModel.
 */

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        SharedPreferences.OnSharedPreferenceChangeListener,
        OnWeatherChangedListener
{

    private String TAG = getClass().getSimpleName();

    private ListView mLocationList;

    private WeatherOps mWeatherOps;
    private LocationAdapter mAdapter;
    private LocationModel mLocationModel;

    PendingIntent mAlarmIntent;
    AlarmManager mAlarmMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a new WeatherOps instance.
        mWeatherOps = WeatherOps.getInstance();
        mWeatherOps.init(this);

        mLocationModel = LocationModel.getInstance();
        mLocationModel.setOnWeatherChangedListener(this);
        mAdapter = new LocationAdapter(this, null, 0);

        mLocationList = (ListView) findViewById(R.id.location_list);
        mLocationList.setAdapter(mAdapter);

        mLocationList.setOnItemClickListener(this);

        mAlarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        setAlarm();

        try {
            // This will initiate LocationModel and that will in the end populate the LocationList
            // in this activity. ToDo: Find a cleaner solution for initiating the LocationList
            mWeatherOps.displayAll();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /** setAlarm
     * setAlarm will result in periodic triggering of the AlarmReceiver which in its turn will
     * retrieve new temperature data from SMHI.
     */
    private void setAlarm() {
        // Get interval from persistent storage
        long interval = getUpdateInterval();

        Intent intent = new Intent(this, AlarmReceiver.class);
        mAlarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        mAlarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + interval, interval, mAlarmIntent);

        String log_msg = String.format("Inexact repeating alarm set, interval = %d min", interval/(60*1000));
        Log.i(TAG, log_msg);
    }

    private void cancelAlarm() {
        if (mAlarmMgr!= null) {
            mAlarmMgr.cancel(mAlarmIntent);
        }
    }

    /** getUpdateInterval
     * getUpdateInterval retrieves the interval set in the Settings activity from persistent
     * storage
     * @return interval in milli seconds
     */
    private long getUpdateInterval() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String intervalStr = sharedPref.getString(SettingsActivity.KEY_PREF_UPDATE_INTERVAL, "60");
        // Interval stored in minutes return value in ms
        return (long) Integer.parseInt(intervalStr)*60000;
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

// ToDo: Should I implement more lifecycle methods?

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }

    /** Called when the user taps the Settings button */
    public void onSettingsClick(View view) {
        Log.i(TAG, "onSettingsClick");
        Intent intent = SettingsActivity.makeIntent(this);
        startActivity(intent);
    }

    /** Called when the user taps the Update button */
    public void onUpdateClick(View view) {
        Log.i(TAG, "onUpdateClick");

        // Triggers a retrieval of data form SMHI by sending a broadcast to the AlarmReceiver.
        Intent intent = new Intent(this, AlarmReceiver.class);
        sendBroadcast(intent);
    }

    @Override
    public  void onItemClick(AdapterView parent, View v, int position, long id) {
        // Not implemented yet
        Log.i(TAG, "ListView - onItemClick, position: " + position);
    }

    /** onWeatherChanged
     * onWeatherChanged is a listener that is called by the LocationModel whenever the
     * WeatherProvider is updated.
     */
    @Override
    public void onWeatherChanged() {
        Log.i(TAG, "onWeatherChanged");
        update_location_list();
        update_info_textview();
    }

    /** update_info_textview
     * update_info_textview updates the information regarding when the temperatures were last
     * updated and if the update was successful or not.
     */
    private void update_info_textview() {
        Date        updateTime = mLocationModel.getTimeForLastUpdate();

        TextView    info = (TextView) findViewById(R.id.into_textview);

        switch (mLocationModel.getState()) {
            case UNINITIALIZED:
                info.setText(R.string.no_locations);
                break;
            case INITIALIZED:
                info.setText(R.string.no_data_received);
                break;
            case UPDATE_FAILED:
                setInfoTextViewWithTime(info, R.string.update_failed, updateTime);
                break;
            case UPDATE_PARTIALLY_OK:
                setInfoTextViewWithTime(info, R.string.update_partially_ok, updateTime);
                break;
            case UPDATE_OK:
                setInfoTextViewWithTime(info, R.string.update_ok, updateTime);
                break;
        }

        // Disable update button if state is UNINITIALIZED
        Button      updateBtn = (Button)findViewById(R.id.update_button);
        updateBtn.setEnabled(mLocationModel.getState() != LocationModel.State.UNINITIALIZED);
    }

    /** update_location_list
     * update_location_list sets a new cursor on the list adapter to display new temperature data.
     */
    private void update_location_list() {
        Log.i(TAG, "update_location_list()");
        mAdapter.changeCursor(mLocationModel.getCursor());
    }

    private void setInfoTextViewWithTime(TextView v, int stringId, Date updateTime) {
        String str = getResources().getString(stringId);
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd - HH:mm:ss", Locale.getDefault())
                .format(updateTime);
        v.setText(str + ": " + timeStamp);
    }

    /** onSharedPreferenceChanged
     * onSharedPreferenceChanged listens for changes in persistent storage.
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsActivity.KEY_PREF_UPDATE_INTERVAL)) {
            // Cancel current alarm and set a new
            cancelAlarm();
            setAlarm();
        }
        else if (key.equals(SettingsActivity.KEY_PREF_FAHRENHEIT)) {
            // Update location list to reflect the newly set temperature unit.
            update_location_list();
        }
    }
}
