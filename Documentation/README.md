# MyWeatherApp
# Introduction
MyWeatherApp is an app designed for the Coursera Capstone MOOC for "Android App Development". Initially it will only contain what is needed for the weekly assignments. Hopefully it will be possible to extend the app with more features that will make the app a favorite weather app for me and others.
## Description
The MyWeatherApp will provide an easy way to provide the current temperature for a number of hardcoded locations in Sweden. The data will be provided by SMHI which is Swedish weather forcasting institute. The main screen will show a row each for these locations with a location name and the associated current temperature.

There shall be a button on the main page that takes you to a settings page. 

### HMI
The picture below illustrates the two activities involved in the HMI flow.

![HMI Flow][hmi_flow]

[hmi_flow]: ./hmi_flow_resized_533x400.jpeg

## Web Service
SMHI provides a service SMHI Open Data Meteorological Observations. 
The service is constructed as a REST API for easy access. 
The Entry point is located at:
 https://opendata-download-metobs.smhi.se/api. 
 
From here a client can traverse down by following links through the levels until a 
specific data resource is available. 

The api is documented at:
https://opendata.smhi.se/apidocs/metobs/index.html

The service provides a number of formats for finding out what data is available. 
I have used JSON for that. In the end the actual data is provided as a .csv file.

### Temperature data
The url for retrieving the temperature data is put together by four different parts:
 * `metObsAPI`: Base url for SMHI's methrological observations. 
 * `parameterKey`: Data type identifier. In this case a value for temperature observations are chosen. 
 * `stationKey`: Station name.
 * `periodName`: Period for requested data. 
 
`metObsAPI + "/version/latest/parameter/" + parameterKey + "/station/" + stationKey + 
"/period/" + periodName + "/data.csv `


#### parameterKey
For temperature observations the following parameters are available.

* Parameter: 1. Air temperature, Instantaneous 1 value/h.
* Parameter: 2. Air temperature, Average 1 day, once every day at 00:00.
* Parameter: 19. Air temperature, Min, once every day.
* Parameter: 20. Air temperature, Max, once every day.
* Parameter: 22. Air temperature, Average 1 month.

Paramter 1 is used in the MyWeather app.

#### stationKey
The following StationId:s are used for the locations of interest:

 * Göteborg: 71420
 * Karlstad Airport: 93220
 * Väderöarna: 81350 
 * Måseskär: 81050
    
#### periodName
The following period names are available:
* latest-hour
* latest-day
* latest-months
* corrected-archive

In MyWeather **latest-hour** will be used. It provides only one value, same value will also be found in latest day.

#### Parse the csv data
The data received as a csv string is parsed to find the temperature info. The following steps are involved in the parsing:

* The string is split up into rows using '\n' as a separator.
* Search for the header row which has a specific signature.
* When the header row is found the next row is split up into fields using ";" as a separator.
* The 3:rd field contains the latest temperature observation as a string.

## Architecture
The picture below shows the architecture of the app.

![Architecture][architecture]

[architecture]: ./AppStructure_resize_400x533.JPG

### Activities
#### MainActivity
The `MainActivity` is the view presenting the current temperature for all locations in a list. 
It also shows the time for the last retrieval of temperatures.
There are two buttons:
1. Update, which starts `WeatherServiece` so that new data is retrieved
2. Settings, which displays the SettingsActivity.

All temperature data is retrieved from `TemperatureProvider` via the LocationModel. 

When the `MainActivity` is created two actions are performed:
1. it starts the `WeatherService` to retrieve the first set of temperatures. 
1. It also uses the `AlarmManager` to schedule periodic retrieval of temperature data.

If the `WeatherProvider` is empty the `MainActivity` will indicate that no locations are set.
 
The `MainActivity` implements the following interfaces:
1. SharedPreferences.OnSharedPreferenceChangeListener.
1. OnWeatherChangedListener (see below).

#### SettingsActivity
The `SettingsActivity` presents a number of options to configure MyWeather. 
1. Displayed temperature unit, °C or °F radio button selection.
1. Interval for data retrieval.
1. Button to initialize the `TemperatureProvider` with a set of hardcoded locations.
1. Button to remove all locations from the 'TemperatureProvider'

#### SettingsFragment
Loads a resource for displaying and modifying the settings via persistent storage.

#### LocationAdapter
The `LocationAdapter` class extends `CursorAdapter` and is used for populating the list in `MainActivity`.

#### LocationModel
The `LocationModel` class holds HMI related information such as status and time for latest data 
retrieval. It gets notified when the `WeatherProvider` has been updated. The `LocationModel` is 
then responsible for notifying the `MainActivity` that the weather data is updated. The class also 
holds the hardcoded data necessary to initialize the provider with a set of stations.

The `LocationModel` is implemented as a singelton.

The `OnWeatherChanged` interface is used for notifying that the weather data is updated.

#### OnWeatherChangedListener
Interface for notifying a listener when the weather data is updated.

### ContentProvider
#### WeatherProvider
The `WeatherProvider` contains a list of all locations and for each location it holds the followinf data:
1. NAME, friendly name of the station.
1. STATION_KEY, id to use when retrieving data from the SMHI web service.
1. TEMPERATURE, the last retrieved temperature value.
1. TEMPERATURE_VALID, boolean to indicate if last attempt to retrieve temperature was successful or not.

#### LocationContract
This contract defines the metadata for the WeatherContentProvider, including the provider's access URIs and its "database" constants.
 
#### WeatherDatabaseHelper
`WeatherDatabaseHelper` is a support class for administrating the SQLite database.

#### LocationRecord
A simple POJO that stores weather related information for locations.

### Service
### WeatherService
The `WeatherService` is a StartedService that retrieves all locations from the `WeatherProvider` 
and then uses the SMHI web service for fetching the current temperatures. Finally it also updates 
the `WeatherProvider` with the new temperatures.

#### JSONParse
`JSONParse` is a support class for parsing the JSON data received from the web service.

### BroadcastReceiver
#### AlarmReceiver
The `AlarmReceiver` is triggered with an interval that can be configured via the `SettingsActivity`. 
The receiver initiates updating of current temperatures by starting the `WeatherService`.  

### Utility Class
#### WeatherOps
`WeatherOps` is a support class for managing the communicaiton with the `WeatherProvider`.



